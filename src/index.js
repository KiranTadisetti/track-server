require('./models/Users') //we dont assign it to any const since mongoose takes care to intialise the schema only once but if we assign it to any varaible there is a chance that it may end up intialise many times 
require('./models/Tracks') //we dont assign it to any const since mongoose takes care to intialise the schema only once but if we assign it to any varaible there is a chance that it may end up intialise many times 

const express =require('express');
const mongoose =require('mongoose');
const authRoutes =require('./routes/authRoutes')
const trackRoutes =require('./routes/trackRoutes')
const bodyParser =require('body-parser');
const requireAuth =require('./middlewares/requireAuth');

const app =express();


app.use(bodyParser.json());
app.use(authRoutes);
app.use(trackRoutes);

const mongoUri='mongodb+srv://admin:kiran@cluster0-yg5za.mongodb.net/test?retryWrites=true&w=majority'

mongoose.connect(mongoUri,{
    useNewUrlParser:true,
    useCreateIndex:true,
    useUnifiedTopology: true
})
mongoose.connection.on('connected',()=>{
    console.log('Connected to mongo instance')
});
mongoose.connection.on('error',(err)=>{
    console.log('Error Connecting to mongo',console.err);
});


app.get('/',requireAuth,(req,res)=>{
    res.send(`your email is ${req.user.email}`);
});

app.listen(3000,()=>{
    console.log('Listening on port 3000');
})